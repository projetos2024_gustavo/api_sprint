const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    if (telefone !== 0) {
      const query = `insert into cliente(telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
                
            )`;

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res
              .status(500)
              .json({ error: "Usuário não cadastrado no banco!!!" });
          }
          console.log("Inserido no Banco!!!");
          res.status(201).json({ messagem: "Usuario criado com sucesso!!!" });
        });
      } catch (error) {
        console.error("Erro ao executar o insert!!!", error);
        res.status(500).json({ error: "Erro interno do servidor!!!" });
      }
    } else {
      res.status(400).json({ mensagem: "o telefone é obrigatorio!!!" });
    }
  }

  static async getAllClientes(req, res) {
    const query = `select * from cliente`;
    try {
      connect.query(query, function (err, data) {
        if (err) {
          console.log(err);
          res
            .status(500)
            .json({ error: "Usuários não encontrados no banco!!" });
          return;
        }
        let clientes = data;
        console.log("Consulta realizada com sucesso!!");
        res.status(201).json({ clientes });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Error interno do servidor!!!" });
    }
  }

  //métodos para selecionar clientes via parametros especificos
  static async getAllClientes2(req, res) {
    //extrair parametros da consulta da URL
    const { filtro, ordenacao, ordem } = req.query;

    //construir a consulta SQL base
    let query = `select * from cliente`;

    //Adicionar a clausula where, quando houver
    if (filtro) {
      //query = query + filtro; - está incorreto
      query += ` where ${filtro}`;
    }

    //Adicionar clausura order by, quando hover
    if(ordenacao){
      query += ` order by ${ordenacao}`;

      //Adicionar a ordem do order (asc ou desc)
      if(ordem){
        query += ` ${ordem}`;
      }

    }

    try {
      connect.query(query, function (err, result) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Usuários não encontrados no banco!!" });
          return;
        }
        console.log("Consulta realizada com sucesso!!");
        res.status(201).json({ result });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Error interno do servidor!!!" });
    }
  }
};
