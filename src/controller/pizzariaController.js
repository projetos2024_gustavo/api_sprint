const { query } = require("express");
const connect = require("../db/connect");

module.exports = class pizzariaController{
    static async listarPedidosPizza(req, res){
        const query = `
        select
            pp.fk_id_pedido as pedido,
            p.nome as pizza,
            pp.quantidade as Qtde,
            round((pp.valor / pp.quantidade), 2) as unitario,
            pp.valor as total
        from
            pizza_pedido pp, pizza p
        where
            pp.fk_id_pizza = p.id_pizza
        order by
            pp.fk_id_pedido;        
        `;

        try{
            connect.query(query, function(err, result){
                if(err){
                    console.log(err);
                    return res.status(500).json({error: "Error ao consultar o banco de dados!!!"})
                }
    
                console.log("Consulta realizada com sucesso!!!");
                return res.status(200).json({result});
            })
    
        }catch(error){
            console.error("Error ao excutar a consulta de pedidos de pizzas:", error);
            return res.status(500).json({error:"Error interno do servidor!!!"});
        }
    }

    static async listarPedidoscomjoinPizza(req, res){
        const query = `
        select
            pp.fk_id_pedido as pedido,
            p.nome as pizza,
            pp.quantidade as Qtde,
            round((pp.valor / pp.quantidade), 2) as unitario,
            pp.valor as total

        from
            pizza_pedido pp INNER JOIN pizza p
        on
            pp.fk_id_pizza = p.id_pizza
        order by
            pp.fk_id_pedido;        
        `;
        

        try{
            connect.query(query, function(err, result){
                if(err){
                    console.log(err);
                    return res.status(500).json({error: "Error ao consultar o banco de dados!!!"})
                }
    
                console.log("Consulta realizada com sucesso!!!");
                return res.status(200).json({result});
            })
    
        }catch(error){
            console.error("Error ao excutar a consulta de pedidos de pizzas:", error);
            return res.status(500).json({error:"Error interno do servidor!!!"});
        }
    }
}