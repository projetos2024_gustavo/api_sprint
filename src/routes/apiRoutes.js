const router = require('express').Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");

router.get("/tables", dbController.getTables);
router.get("/tables/description", dbController.getDescription);
router.post("/postcliente/", clienteController.createCliente);
router.get("/getClientes/", clienteController.getAllClientes);
router.get("/getClientes2/", clienteController.getAllClientes2);
router.get("/listarPedidosPizza/", pizzariaController.listarPedidosPizza);
router.get("/listarPedidoscomjoinPizza/", pizzariaController.listarPedidoscomjoinPizza);




module.exports = router